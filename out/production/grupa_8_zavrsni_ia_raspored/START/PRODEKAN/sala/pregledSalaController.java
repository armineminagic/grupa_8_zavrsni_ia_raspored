package START.PRODEKAN.sala;

import START.ENTITETI.sala;
import START.ENTITETI.zgrada;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;

public class pregledSalaController implements Initializable {

    @FXML private TableView table;

    @FXML private TableColumn<sala, String> idSala;
    @FXML private TableColumn<sala, String> nazSala;
    @FXML private TableColumn<sala, String> oznSala;
    @FXML private TableColumn<sala, String> idZgrada;
    @FXML private TableColumn<zgrada, String> nazivZgrada;


    ObservableList<sala> sale = FXCollections.observableArrayList();
    ObservableList<String> sala = FXCollections.observableArrayList();
    //ObservableList<zgrada> zgrade = FXCollections.observableArrayList();

    public void initialize(URL url, ResourceBundle rb) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ia_raspored", "root", "!@bazepodataka1234");

            String upit1 = "SELECT * FROM sala INNER JOIN zgrada ON sala.idZgrada = zgrada.idZgrada";
            Statement statement1 = connection.createStatement();
            ResultSet resultSet1 = statement1.executeQuery(upit1);

            /*String upit2 = "SELECT DISTINCT nazivZgrada FROM zgrada";
            Statement statement2 = connection.createStatement();
            ResultSet resultSet2 = statement2.executeQuery(upit2);*/

            while (resultSet1.next()) {
                sale.add(new sala(resultSet1.getInt(1), resultSet1.getString(2), resultSet1.getString(3), resultSet1.getInt(4)));
                sala.add(resultSet1.getString(6));
            }

        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        idSala.setCellValueFactory(new PropertyValueFactory<>("idSala"));
        nazSala.setCellValueFactory(new PropertyValueFactory<>("nazSala"));
        oznSala.setCellValueFactory(new PropertyValueFactory<>("oznSala"));
        idZgrada.setCellValueFactory(new PropertyValueFactory<>("idZgrada"));
        nazivZgrada.setCellValueFactory(new PropertyValueFactory<>("nazivZgrada"));
        table.setItems(sale);
    }

    public void nazad(ActionEvent actionEvent) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource("sala.fxml"));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle("Dodajte salu");
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
