package START.PRODEKAN.grupa;

import START.ENTITETI.grupa;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;

public class pregledGrupaController implements Initializable {

    @FXML
    private TableView table;
    @FXML private TableColumn<grupa, String> idGrupa;
    @FXML private TableColumn<grupa, String> nazGrupa;
    @FXML private TableColumn<grupa, String> brojStud;
    @FXML private TableColumn<grupa, String> sifNastavnik;


    ObservableList<grupa> grupe = FXCollections.observableArrayList();

    public void initialize(URL url, ResourceBundle rb) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ia_raspored", "root", "!@bazepodataka1234");

            String upit1 = "SELECT * FROM grupa";
            Statement statement1 = connection.createStatement();
            ResultSet resultSet1 = statement1.executeQuery(upit1);

            while (resultSet1.next()) {
                grupe.add(new grupa(resultSet1.getInt(1), resultSet1.getString(2), resultSet1.getInt(3), resultSet1.getInt(4)));
            }
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        idGrupa.setCellValueFactory(new PropertyValueFactory<>("idGrupa"));
        nazGrupa.setCellValueFactory(new PropertyValueFactory<>("nazGrupa"));
        brojStud.setCellValueFactory(new PropertyValueFactory<>("brojStud"));
        sifNastavnik.setCellValueFactory(new PropertyValueFactory<>("sifNastavnik"));
        table.setItems(grupe);
    }







    public void nazad(ActionEvent actionEvent) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource("grupa.fxml"));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle("Dodajte grupu");
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
