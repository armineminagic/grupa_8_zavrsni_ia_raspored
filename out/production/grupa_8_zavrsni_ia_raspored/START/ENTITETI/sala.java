package START.ENTITETI;

public class sala {
    private int idSala;
    private String nazSala;
    private String oznSala;
    private int idZgrada;

    public sala(){}

    public sala(int idSala, String nazSala, String oznSala, int idZgrada){
        this.idSala = idSala;
        this.nazSala = nazSala;
        this.oznSala = oznSala;
        this.idZgrada = idZgrada;
    }

    public int getIdSala() {
        return idSala;
    }

    public void setIdSala(int idSala) {
        this.idSala = idSala;
    }

    public String getNazSala() {
        return nazSala;
    }

    public void setNazSala(String nazSala) {
        this.nazSala = nazSala;
    }

    public String getOznSala() {
        return oznSala;
    }

    public void setOznSala(String oznSala) {
        this.oznSala = oznSala;
    }

    public int getIdZgrada() {
        return idZgrada;
    }

    public void setIdZgrada(int idZgrada) {
        this.idZgrada = idZgrada;
    }
}
