package START.ENTITETI;
import java.sql.Date;
import java.sql.Time;

public class rezervacija {
    private int idRezervacija;
    private String nazivRezervacija;
    private Date datumRezervacija;
    private Time pocetak;
    private Time kraj;
    private int idSala;
    private int sifNastavnik;

    public rezervacija(){}

    public rezervacija(int idRezervacija, String nazivRezervacija, Date datumRezervacija,
                       Time pocetak, Time kraj, int idSala, int sifNastavnik){
        this.idRezervacija = idRezervacija;
        this.nazivRezervacija = nazivRezervacija;
        this.datumRezervacija = datumRezervacija;
        this.pocetak = pocetak;
        this.kraj = kraj;
        this.idSala = idSala;
        this.sifNastavnik = sifNastavnik;
    }

    public int getIdRezervacija() {
        return idRezervacija;
    }

    public void setIdRezervacija(int idRezervacija) {
        this.idRezervacija = idRezervacija;
    }

    public Date getDatumRezervacija() {
        return datumRezervacija;
    }

    public void setDatumRezervacija(Date datumRezervacija) {
        this.datumRezervacija = datumRezervacija;
    }

    public String getNazivRezervacija() {
        return nazivRezervacija;
    }

    public void setNazivRezervacija(String nazivRezervacija) {
        this.nazivRezervacija = nazivRezervacija;
    }

    public Time getPocetak() {
        return pocetak;
    }

    public void setPocetak(Time pocetak) {
        this.pocetak = pocetak;
    }

    public Time getKraj() {
        return kraj;
    }

    public void setKraj(Time kraj) {
        this.kraj = kraj;
    }

    public int getIdSala() {
        return idSala;
    }

    public void setIdSala(int idSala) {
        this.idSala = idSala;
    }

    public int getSifNastavnik() {
        return sifNastavnik;
    }

    public void setSifNastavnik(int sifNastavnik) {
        this.sifNastavnik = sifNastavnik;
    }
}
