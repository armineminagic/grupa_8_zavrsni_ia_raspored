package START.ENTITETI;

import java.util.List;

public class semestar {

    private String nazivSemestar;
    private String oznSemestar;

    public semestar(){}

    public semestar(String nazivSemestar, String oznSemestar){
        this.nazivSemestar = nazivSemestar;
        this.oznSemestar = oznSemestar;
    }

    public String getOznSemestar() {
        return oznSemestar;
    }

    public void setOznSemestar(String oznSemestar) {
        this.oznSemestar = oznSemestar;
    }

    public String getNazivSemestar() {
        return nazivSemestar;
    }

    public void setNazivSemestar(String nazivSemestar) {
        this.nazivSemestar = nazivSemestar;
    }


}

