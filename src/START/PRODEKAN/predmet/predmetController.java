package START.PRODEKAN.predmet;


import START.ENTITETI.usmjerenje;
import START.PRODEKAN.grupa.grupaController;
import START.PRODEKAN.usmjerenje.*;

import START.ENTITETI.semestar;
import START.PRODEKAN.semestar.*;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;

public class predmetController implements Initializable {

    public void pregledPredmeta(ActionEvent actionEvent) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource("../predmet/pregledPredmeta.fxml"));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle("Pregled predmeta");
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void nazad(ActionEvent actionEvent) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource("../sampleProdekan.fxml"));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle("Prodekan");
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    ObservableList list1 = FXCollections.observableArrayList();
    ObservableList list2 = FXCollections.observableArrayList();

    public TextField textfieldnazPredmet;
    public TextField textfieldskrPredmet;
    public ChoiceBox<String> choiceboxsemestar;
    static int idzaPredmet = 20;

    public void loadData(){
        list1.removeAll(list1);
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ia_raspored", "root", "!@bazepodataka1234");

            String upit1 = "SELECT * FROM semestar";

            Statement statement1 = connection.createStatement();
            ResultSet resultSet1 = statement1.executeQuery(upit1);

            while (resultSet1.next()) {
                list1.add(resultSet1.getString(1));
            }

        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        choiceboxsemestar.getItems().addAll(list1);
    }

    public void buttonPotvrdi(ActionEvent actionEvent) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ia_raspored", "root", "!@bazepodataka1234");

            int idPredmetnovi = ++idzaPredmet;

            String nazPredmetnovi = textfieldnazPredmet.getText();
            String skrPredmetnovi = textfieldskrPredmet.getText();
            String nazSemestarS = choiceboxsemestar.getValue();

            String upit = "INSERT INTO predmet(sifPredmet, nazPredmet, skrPredmet, nazivSemestar) VALUES("
                    + idPredmetnovi + ", '" + nazPredmetnovi + "', '" + skrPredmetnovi + "', '" + nazSemestarS + "');";

            Statement statement = connection.createStatement();
            statement.execute(upit);
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        loadData();
    }
}