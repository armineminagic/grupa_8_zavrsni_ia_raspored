package START.PRODEKAN.zgrada;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class zgradaController {

    public void pregledZgrada(ActionEvent actionEvent){

        try {
            Parent view2 = FXMLLoader.load(getClass().getResource("../zgrada/pregledZgrada.fxml"));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle("Pregled zgrada");
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void nazad(ActionEvent actionEvent) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource("../sampleProdekan.fxml"));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle("Prodekan");
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static int idzaZgradu = 5;
    public TextField textfieldnaziv;
    public TextField textfieldoznaka;

    public void buttonPotvrdi(ActionEvent actionEvent) {

        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ia_raspored", "root", "!@bazepodataka1234");

            int idZnovi = ++idzaZgradu;
            String nazivZnovi = textfieldnaziv.getText();
            String oznZnovi = textfieldoznaka.getText();


            String upit = "INSERT INTO zgrada(idZgrada, nazivZgrada, oznZgrada) " +
                    "VALUES(" + idZnovi  + ", '" + nazivZnovi + "', '" + oznZnovi  +  "');";


            Statement statement = connection.createStatement();
            statement.execute(upit);
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
}