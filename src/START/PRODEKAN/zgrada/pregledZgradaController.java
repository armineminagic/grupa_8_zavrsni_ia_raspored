package START.PRODEKAN.zgrada;

import START.ENTITETI.grupa;
import START.ENTITETI.zgrada;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;

public class pregledZgradaController implements Initializable {

        @FXML private TableView table;
        @FXML private TableColumn<zgrada, String> idZgrada;
        @FXML private TableColumn<zgrada, String> nazivZgrada;
        @FXML private TableColumn<zgrada, String> oznZgrada;


        ObservableList<zgrada> zgrade = FXCollections.observableArrayList();
        Connection connection;
        ResultSet resultSet1;

        public void initialize(URL url, ResourceBundle rb) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ia_raspored", "root", "!@bazepodataka1234");

                String upit1 = "SELECT * FROM zgrada";
                Statement statement1 = connection.createStatement();
                resultSet1 = statement1.executeQuery(upit1);

                while (resultSet1.next()) {
                    zgrade.add(new zgrada(resultSet1.getInt(1), resultSet1.getString(2), resultSet1.getString(3)));
                }
            }
        catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            idZgrada.setCellValueFactory(new PropertyValueFactory<>("idZgrada"));
            nazivZgrada.setCellValueFactory(new PropertyValueFactory<>("nazivZgrada"));
            oznZgrada.setCellValueFactory(new PropertyValueFactory<>("oznZgrada"));
            table.setItems(zgrade);
        }


    public void Delete () throws ClassNotFoundException, SQLException {
        try {
            zgrada zgrada= (zgrada)table.getSelectionModel().getSelectedItem();

            String sql = "DELETE FROM zgrada WHERE  idZgrada= " + zgrada.getIdZgrada();
            System.out.println(sql);
            Statement statement = connection.createStatement();

            statement.executeUpdate(sql);
            zgrade.remove(zgrada);
            UpdateTable();
            JOptionPane.showMessageDialog(null, "Izbrisi");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void UpdateTable(){
        idZgrada.setCellValueFactory(new PropertyValueFactory<>("idZgrada"));
        nazivZgrada.setCellValueFactory(new PropertyValueFactory<>("nazivZgrada"));
        oznZgrada.setCellValueFactory(new PropertyValueFactory<>("oznZgrada"));
        table.setItems(zgrade);
    }


    public void nazad(ActionEvent actionEvent) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource("zgrada.fxml"));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle("Dodajte zgradu");
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
