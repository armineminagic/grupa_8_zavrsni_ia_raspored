package START.PRODEKAN.grupa;


import START.ENTITETI.grupa;
import START.PRODEKAN.grupa.*;

import START.ENTITETI.nastavnik;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.scene.Node;

import javax.naming.Name;
import java.awt.*;
import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;

import static java.lang.Integer.parseInt;

public class grupaController implements Initializable {

    public void nazad(ActionEvent actionEvent) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource("../sampleProdekan.fxml"));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle("Prodekan");
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void pregledGrupa(ActionEvent actionEvent) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource("../grupa/pregledGrupa.fxml"));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle("Prodekan");
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    ObservableList list1 = FXCollections.observableArrayList();

    public TextField textFieldnazGrupe;
    public TextField textFieldbrojStudenata;
    public ChoiceBox<String> choiceboxNastavnici;
    static int idzaGrupu = 7;

    public void loadData(){
        list1.removeAll(list1);
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ia_raspored", "root", "!@bazepodataka1234");

            String upit1 = "SELECT * FROM nastavnik";

            Statement statement1 = connection.createStatement();
            ResultSet resultSet1 = statement1.executeQuery(upit1);

            while (resultSet1.next()) {
                String l = resultSet1.getString(2) + " " + resultSet1.getString(3);
                list1.add(l);
            }

        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        choiceboxNastavnici.getItems().addAll(list1);

    }



    public class NamesConverter {

        private List<String> titlesBefore = new ArrayList<>();
        private List<String> titlesAfter = new ArrayList<>();
        private String firstName = "";
        private String lastName = "";
        private List<String> middleNames = new ArrayList<>();

        public NamesConverter(String name) {
            String[] words = name.split(" ");
            boolean isTitleAfter = false;
            boolean isFirstName = false;

            int length = words.length;
            for (String word : words) {
                if (word.charAt(word.length() - 1) == '.') {
                    if (isTitleAfter) {
                        titlesAfter.add(word);
                    } else {
                        titlesBefore.add(word);
                    }
                } else {
                    isTitleAfter = true;
                    if (isFirstName == false) {
                        firstName = word;
                        isFirstName = true;
                    } else {
                        middleNames.add(word);
                    }
                }
            }
            if (middleNames.size() > 0) {
                lastName = middleNames.get(middleNames.size() - 1);
                middleNames.remove(lastName);
            }
        }

        public List<String> getTitlesBefore() {
            return titlesBefore;
        }

        public List<String> getTitlesAfter() {
            return titlesAfter;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public List<String> getMiddleNames() {
            return middleNames;
        }

    }





    public void buttonPotvrdi(ActionEvent actionEvent) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ia_raspored", "root", "!@bazepodataka1234");

            int idGrupanovi = ++idzaGrupu;

            String nazGrupenovi = textFieldnazGrupe.getText();
            String brojStudenatanovi = textFieldbrojStudenata.getText();
            int brojStudenatanoviI = Integer.parseInt(brojStudenatanovi);

            String nazNastavnikS = choiceboxNastavnici.getValue();
            NamesConverter ns = new NamesConverter(nazNastavnikS);
            String imeNastavnikS = ns.getFirstName();
            String prezNastavikS = ns.getLastName();

            String upit1 = "SELECT * FROM nastavnik WHERE imeNastavnik='" + imeNastavnikS + "' AND prezNastavnik='" + prezNastavikS + "';";
            Statement statementN = connection.createStatement();
            ResultSet resultSetN = statementN.executeQuery(upit1);
            int sifNastavniknovi = 0;
            if(resultSetN.first()) {
                sifNastavniknovi = resultSetN.getInt(1);
            }

            String upit = "INSERT INTO grupa(idGrupa, nazGrupa, brojStud, sifNastavnik) VALUES("
                    + idGrupanovi  + ", '" + nazGrupenovi + "', " + brojStudenatanoviI + ", " + sifNastavniknovi + ");";


            Statement statement = connection.createStatement();
            statement.execute(upit);
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        loadData();
    }
}

