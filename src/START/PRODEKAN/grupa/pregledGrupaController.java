package START.PRODEKAN.grupa;

import START.ENTITETI.grupa;
import START.PROFESOR.obrisi_rezervaciju.mysqlconnect;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;

public class pregledGrupaController implements Initializable {

    @FXML
    private TableView table;
    @FXML private TableColumn<grupa, Integer> idGrupa;
    @FXML private TableColumn<grupa, String> nazGrupa;
    @FXML private TableColumn<grupa, String> brojStud;
    @FXML private TableColumn<grupa, String> sifNastavnik;


    ObservableList<grupa> grupe = FXCollections.observableArrayList();
    Connection connection;
    ResultSet resultSet1;
    public void initialize(URL url, ResourceBundle rb) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ia_raspored", "root", "!@bazepodataka1234");

            String upit1 = "SELECT * FROM grupa";
            Statement statement1 = connection.createStatement();
            resultSet1 = statement1.executeQuery(upit1);

            while (resultSet1.next()) {
                grupe.add(new grupa(resultSet1.getInt(1), resultSet1.getString(2), resultSet1.getInt(3), resultSet1.getInt(4)));
            }
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        idGrupa.setCellValueFactory(new PropertyValueFactory<>("idGrupa"));
        nazGrupa.setCellValueFactory(new PropertyValueFactory<>("nazGrupa"));
        brojStud.setCellValueFactory(new PropertyValueFactory<>("brojStud"));
        sifNastavnik.setCellValueFactory(new PropertyValueFactory<>("sifNastavnik"));
        table.setItems(grupe);
    }

    public void Delete () throws ClassNotFoundException, SQLException {
        try {
            grupa grupa= (grupa)table.getSelectionModel().getSelectedItem();

            String sql = "DELETE FROM grupa WHERE idGrupa = " + grupa.getIdGrupa();
            System.out.println(sql);
            Statement statement = connection.createStatement();

            statement.executeUpdate(sql);
            grupe.remove( grupa);
            UpdateTable();
            JOptionPane.showMessageDialog(null, "Izbrisi");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

public void UpdateTable(){
    idGrupa.setCellValueFactory(new PropertyValueFactory<>("idGrupa"));
    nazGrupa.setCellValueFactory(new PropertyValueFactory<>("nazGrupa"));
    brojStud.setCellValueFactory(new PropertyValueFactory<>("brojStud"));
    sifNastavnik.setCellValueFactory(new PropertyValueFactory<>("sifNastavnik"));
    table.setItems(grupe);
}

    public void nazad(ActionEvent actionEvent) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource("grupa.fxml"));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle("Dodajte grupu");
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
