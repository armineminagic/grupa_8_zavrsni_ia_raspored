package START.PRODEKAN.sala;


import START.ENTITETI.zgrada;
import START.PRODEKAN.zgrada.*;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;

public class salaController implements Initializable  {

    public void pregledSala(ActionEvent actionEvent) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource("../sala/pregledSala.fxml"));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle("Pregled sala");
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void nazad(ActionEvent actionEvent) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource("../sampleProdekan.fxml"));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle("Prodekan");
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    ObservableList list1 = FXCollections.observableArrayList();

    public TextField textfieldnazivsale;
    public TextField textfieldoznakasale;
    public ChoiceBox<String> choiceboxzgrada;
    static int idzaSalu = 15;


    public void loadData(){
        list1.removeAll(list1);
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ia_raspored", "root", "!@bazepodataka1234");

            String upit1 = "SELECT * FROM zgrada";

            Statement statement1 = connection.createStatement();
            ResultSet resultSet1 = statement1.executeQuery(upit1);

            while (resultSet1.next()) {
                list1.add(resultSet1.getString(2));
            }

        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        choiceboxzgrada.getItems().addAll(list1);
    }


    public void buttonPotvrdi(ActionEvent actionEvent) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ia_raspored", "root", "!@bazepodataka1234");

            int idSalanovi = ++idzaSalu;

            String nazSalenovi = textfieldnazivsale.getText();
            String oznSalenovi = textfieldoznakasale.getText();

            String nazZgradeS = choiceboxzgrada.getValue();
            String upit2 = "SELECT * FROM zgrada WHERE nazivZgrada='" + nazZgradeS + "';";
            Statement statementZ = connection.createStatement();
            ResultSet resultSetZ = statementZ.executeQuery(upit2);
            int idZgradanovi = 4;
            if(resultSetZ.first()) {
                idZgradanovi = resultSetZ.getInt(1);
            }

            String upit = "INSERT INTO sala(idSala, nazSala, oznSala, idZgrada) VALUES("
                    + idSalanovi + ", '" + nazSalenovi + "', '" + oznSalenovi + "', " + idZgradanovi + ");";

            Statement statement = connection.createStatement();
            statement.execute(upit);
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


    }
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        loadData();
    }
}