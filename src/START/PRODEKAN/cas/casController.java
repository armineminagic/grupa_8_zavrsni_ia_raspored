package START.PRODEKAN.cas;

import START.ENTITETI.cas;
import START.ENTITETI.grupa;
import START.PRODEKAN.grupa.*;
import START.ENTITETI.predmet;
import START.PRODEKAN.predmet.*;
import START.ENTITETI.sala;
import START.PRODEKAN.sala.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;

import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.time.LocalDate;
import java.util.ResourceBundle;

import static java.lang.Integer.parseInt;


public class casController  implements Initializable {

    public void pregledCasova(ActionEvent actionEvent) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource("../cas/pregledCasova.fxml"));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle("Pregled casova");
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void nazad(ActionEvent actionEvent) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource("../sampleProdekan.fxml"));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle("Prodekan");
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    ObservableList list1 = FXCollections.observableArrayList();
    ObservableList list2 = FXCollections.observableArrayList();
    ObservableList list3 = FXCollections.observableArrayList();
    ObservableList list4 = FXCollections.observableArrayList();

    public TextField textfieldpocetak;
    public TextField textfieldkraj;
    public DatePicker datapickerdan;
    public ChoiceBox<String> choiceboxtip;
    public ChoiceBox<String> choiceboxsala;
    public ChoiceBox<String> choiceboxgrupa;
    public ChoiceBox<String> choiceboxpredmet;
    static int idzaCas = 17;


    public void loadData(){
        list1.removeAll(list1);
        list2.removeAll(list2);
        list3.removeAll(list3);
        list4.removeAll(list4);
        String a="Predavanja";
        String b="AV";
        String c="LV";
        list1.addAll(a,b,c);
        choiceboxtip.getItems().addAll(list1);
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ia_raspored", "root", "!@bazepodataka1234");

            String upit1 = "SELECT * FROM grupa";
            String upit2 = "SELECT * FROM predmet";
            String upit3 = "SELECT * FROM sala";


            Statement statement1 = connection.createStatement();
            ResultSet resultSet1 = statement1.executeQuery(upit1);
            Statement statement2 = connection.createStatement();
            ResultSet resultSet2 = statement2.executeQuery(upit2);
            Statement statement3 = connection.createStatement();
            ResultSet resultSet3 = statement3.executeQuery(upit3);

            while (resultSet1.next()) {
                list2.add(resultSet1.getString(2));
            }
            while (resultSet2.next()) {
                list4.add(resultSet2.getString(2));
            }
            while (resultSet3.next()) {
                list3.add(resultSet3.getString(2));
            }

        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        choiceboxgrupa.getItems().addAll(list2);
        choiceboxpredmet.getItems().addAll(list4);
        choiceboxsala.getItems().addAll(list3);
    }





    public void buttonPotvrdi(ActionEvent actionEvent)  {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ia_raspored", "root", "!@bazepodataka1234");


            int idCasnovi = ++idzaCas;
            String tipCasnovi = choiceboxtip.getValue();

            LocalDate danL = datapickerdan.getValue();
            String dannovi = danL.toString();


            String pocetaknovi = textfieldpocetak.getText();
            String krajnovi = textfieldkraj.getText();

            String nazGrupas=choiceboxgrupa.getValue();
            String upit1 = "SELECT * FROM grupa WHERE nazGrupa='" + nazGrupas + "';";
            Statement statementG = connection.createStatement();
            ResultSet resultSetG = statementG.executeQuery(upit1);
            int idGrupanovi = 0;
            if(resultSetG.first()) {
                idGrupanovi = resultSetG.getInt(1);
            }


            String nazPredmets=choiceboxpredmet.getValue();
            String upit2 = "SELECT * FROM predmet WHERE nazPredmet='" + nazPredmets + "';";
            Statement statementP = connection.createStatement();
            ResultSet resultSetP = statementP.executeQuery(upit2);
            int sifPredmetnovi = 0;
            if(resultSetP.first()) {
                sifPredmetnovi = resultSetP.getInt(1);
            }


            String nazSalas=choiceboxsala.getValue();
            String upit3 = "SELECT * FROM sala WHERE nazSala='" + nazSalas + "';";
            Statement statementS = connection.createStatement();
            ResultSet resultSetS = statementS.executeQuery(upit3);
            int idSalanovi = 0;
            if(resultSetS.first()) {
                idSalanovi = resultSetS.getInt(1);
            }


            String upit = "INSERT INTO cas(idCas, tipCas, dan, pocetak, kraj, idGrupa, sifPredmet, idSala) VALUES("
                    + idCasnovi  + ", '" + tipCasnovi + "', '" + dannovi + "', '" + pocetaknovi + "', '" +
                    krajnovi + "', " + idGrupanovi + ", " + sifPredmetnovi + ", " + idSalanovi + ");";


            Statement statement = connection.createStatement();
            statement.execute(upit);
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        loadData();
    }

}
