package START.PRODEKAN.cas;

import START.ENTITETI.cas;
import START.ENTITETI.grupa;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;

public class pregledCasovaController implements Initializable {


    @FXML
    private TableView table;
    @FXML private TableColumn<cas, String> idCas;
    @FXML private TableColumn<cas, String> tipCas;
    @FXML private TableColumn<cas, String> dan;
    @FXML private TableColumn<cas, String> pocetak;
    @FXML private TableColumn<cas, String> kraj;
    @FXML private TableColumn<cas, String> idGrupa;
    @FXML private TableColumn<cas, String> sifPredmet;
    @FXML private TableColumn<cas, String> idSala;



    ObservableList<cas> casovi = FXCollections.observableArrayList();
    Connection connection;
    ResultSet resultSet1;

    public void initialize(URL url, ResourceBundle rb) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ia_raspored", "root", "!@bazepodataka1234");

            String upit1 = "SELECT * FROM cas";
            Statement statement1 = connection.createStatement();
            resultSet1 = statement1.executeQuery(upit1);

            while (resultSet1.next()) {
                casovi.add(new cas(resultSet1.getInt(1), resultSet1.getString(2), resultSet1.getString(3),
                        resultSet1.getString(4), resultSet1.getString(5), resultSet1.getInt(6),
                        resultSet1.getInt(7), resultSet1.getInt(8)));
            }
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        idCas.setCellValueFactory(new PropertyValueFactory<>("idCas"));
        tipCas.setCellValueFactory(new PropertyValueFactory<>("tipCas"));
        dan.setCellValueFactory(new PropertyValueFactory<>("dan"));
        pocetak.setCellValueFactory(new PropertyValueFactory<>("pocetak"));
        kraj.setCellValueFactory(new PropertyValueFactory<>("kraj"));
        idGrupa.setCellValueFactory(new PropertyValueFactory<>("idGrupa"));
        sifPredmet.setCellValueFactory(new PropertyValueFactory<>("sifPredmet"));
        idSala.setCellValueFactory(new PropertyValueFactory<>("idSala"));


        table.setItems(casovi);
    }

    public void Delete () throws ClassNotFoundException, SQLException {
        try {
            cas cas = (cas)table.getSelectionModel().getSelectedItem();

            String sql = "DELETE FROM cas WHERE idCas = " + cas.getIdCas();
            System.out.println(sql);
            Statement statement = connection.createStatement();

            statement.executeUpdate(sql);
            casovi.remove(cas);
            UpdateTable();
            JOptionPane.showMessageDialog(null, "Izbrisi");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void UpdateTable(){
        idCas.setCellValueFactory(new PropertyValueFactory<>("idCas"));
        tipCas.setCellValueFactory(new PropertyValueFactory<>("tipCas"));
        dan.setCellValueFactory(new PropertyValueFactory<>("dan"));
        pocetak.setCellValueFactory(new PropertyValueFactory<>("pocetak"));
        kraj.setCellValueFactory(new PropertyValueFactory<>("kraj"));
        idGrupa.setCellValueFactory(new PropertyValueFactory<>("idGrupa"));
        sifPredmet.setCellValueFactory(new PropertyValueFactory<>("sifPredmet"));
        idSala.setCellValueFactory(new PropertyValueFactory<>("idSala"));

        table.setItems(casovi);
    }


    public void nazad(ActionEvent actionEvent) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource("cas.fxml"));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle("Odaberite cas");
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}