package START.PRODEKAN.uposlenik;

import START.PRODEKAN.grupa.grupaController;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;

public class uposlenikController {


    public void pregledUposlenika(ActionEvent actionEvent)  {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource("../uposlenik/pregledUposlenika.fxml"));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle("Prodekan");
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void nazad(ActionEvent actionEvent) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource("../sampleProdekan.fxml"));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle("Prodekan");
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    static int idzaUposlenika = 36;
    public TextField textfieldime;
    public TextField textfieldprezime;
    public TextField textfieldjmbg;
    public TextField textfieldprodekan;
    public TextField textfieldkime;
    public TextField textfieldksifra;
    public TextField textfieldstatus;




    public void buttonPotvrdi(ActionEvent actionEvent) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ia_raspored", "root", "!@bazepodataka1234");

            int idUposleniknovi = ++idzaUposlenika;
            String imeUnovi = textfieldime.getText();
            String prezUnovi = textfieldprezime.getText();
            String jmbgUnoviS = textfieldjmbg.getText();
            long jmbgUnovi = Long.parseLong(jmbgUnoviS);
            String statusUnovi = textfieldstatus.getText();
            String prodekanUnoviS = textfieldprodekan.getText();
            boolean prodekanUnovi;
            if(prodekanUnoviS == "true") prodekanUnovi = true;
            else prodekanUnovi = false;
            String kimeUnovi = textfieldkime.getText();
            String ksifraUnovi = textfieldksifra.getText();

            String upit = "INSERT INTO nastavnik(sifNastavnik, imeNastavnik, prezNastavnik, jmbgNastavnik, status, prodekan, username, password) " +
                    "VALUES(" + idUposleniknovi  + ", '" + imeUnovi + "', '" + prezUnovi + "', " + jmbgUnovi +
                    ", '" + statusUnovi + "', " + prodekanUnovi + ", '" + kimeUnovi + "', '" +  ksifraUnovi + "');";


            Statement statement = connection.createStatement();
            statement.execute(upit);
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}