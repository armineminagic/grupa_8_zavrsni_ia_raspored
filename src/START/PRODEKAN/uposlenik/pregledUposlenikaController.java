package START.PRODEKAN.uposlenik;


import START.ENTITETI.grupa;
import START.ENTITETI.nastavnik;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;

public class pregledUposlenikaController implements Initializable {

    @FXML
    private TableView table;
    @FXML private TableColumn<nastavnik, String> sifNastavnik;
    @FXML private TableColumn<nastavnik, String> imeNastavnik;
    @FXML private TableColumn<nastavnik, String> prezNastavnik;
    @FXML private TableColumn<nastavnik, String> jmbgNastavnik;
    @FXML private TableColumn<nastavnik, String> status;
    @FXML private TableColumn<nastavnik, String> prodekan;
    @FXML private TableColumn<nastavnik, String> username;
    @FXML private TableColumn<nastavnik, String> password;


    ObservableList<nastavnik> nastavnici = FXCollections.observableArrayList();

    Connection connection;
    ResultSet resultSet1;

    public void initialize(URL url, ResourceBundle rb) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ia_raspored", "root", "!@bazepodataka1234");

            String upit1 = "SELECT * FROM nastavnik";
            Statement statement1 = connection.createStatement();
            resultSet1 = statement1.executeQuery(upit1);

            while (resultSet1.next()) {
                nastavnici.add(new nastavnik(resultSet1.getInt(1), resultSet1.getString(2), resultSet1.getString(3), resultSet1.getLong(4),
                        resultSet1.getString(5), resultSet1.getBoolean(6), resultSet1.getString(7), resultSet1.getString(8)));
            }
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        sifNastavnik.setCellValueFactory(new PropertyValueFactory<>("sifNastavnik"));
        imeNastavnik.setCellValueFactory(new PropertyValueFactory<>("imeNastavnik"));
        prezNastavnik.setCellValueFactory(new PropertyValueFactory<>("prezNastavnik"));
        jmbgNastavnik.setCellValueFactory(new PropertyValueFactory<>("jmbgNastavnik"));
        status.setCellValueFactory(new PropertyValueFactory<>("status"));
        prodekan.setCellValueFactory(new PropertyValueFactory<>("prodekan"));
        username.setCellValueFactory(new PropertyValueFactory<>("username"));
        password.setCellValueFactory(new PropertyValueFactory<>("password"));
        table.setItems(nastavnici);
    }

    public void Delete () throws ClassNotFoundException, SQLException {
        try {
            nastavnik nastavnik= (nastavnik)table.getSelectionModel().getSelectedItem();

            String sql = "DELETE FROM nastavnik WHERE sifNastavnik= " + nastavnik.getSifNastavnik();
            System.out.println(sql);
            Statement statement = connection.createStatement();

            statement.executeUpdate(sql);
            nastavnici.remove(nastavnik);
            UpdateTable();
            JOptionPane.showMessageDialog(null, "Izbrisi");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void UpdateTable(){
        sifNastavnik.setCellValueFactory(new PropertyValueFactory<>("sifNastavnik"));
        imeNastavnik.setCellValueFactory(new PropertyValueFactory<>("imeNastavnik"));
        prezNastavnik.setCellValueFactory(new PropertyValueFactory<>("prezNastavnik"));
        jmbgNastavnik.setCellValueFactory(new PropertyValueFactory<>("jmbgNastavnik"));
        status.setCellValueFactory(new PropertyValueFactory<>("status"));
        prodekan.setCellValueFactory(new PropertyValueFactory<>("prodekan"));
        username.setCellValueFactory(new PropertyValueFactory<>("username"));
        password.setCellValueFactory(new PropertyValueFactory<>("password"));
        table.setItems(nastavnici);
    }
    public void nazad(ActionEvent actionEvent) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource("uposlenik.fxml"));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle("Dodajte uposlenika");
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
