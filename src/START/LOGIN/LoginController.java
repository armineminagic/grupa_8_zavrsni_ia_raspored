package START.LOGIN;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.stage.Stage;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;

import START.ENTITETI.nastavnik;


public class LoginController implements Initializable {
    Collection<nastavnik> nastavnici = new ArrayList<nastavnik>();
    String username;
    String password;

    public static List<Stage> stagev = new ArrayList<Stage>();
    public static List<String> fxmlv = new ArrayList<String>();

    public static Stage stage;

    public void Odaberi(ActionEvent actionEvent) {
        String text = ((Button) actionEvent.getSource()).getText();;
        stagev.add((Stage) ((Node) actionEvent.getSource()).getScene().getWindow());
        fxmlv.add("../LOGIN/sampleLogin.fxml");

        System.out.println(text);
        if (text.equals("Prijavi se")) {
            try {
                login();
                for (nastavnik n : nastavnici) {
                    if (n.isProdekan()) {
                        akcija(actionEvent, "Prodekan", "../PRODEKAN/sampleProdekan.fxml");
                    } else {
                        akcija(actionEvent,username, "../PROFESOR/sampleProfesor.fxml");
                    }
                }
            } catch(Exception throwables){
                throwables.printStackTrace();
            }
        } else if (text.equals("Natrag")) {
            akcija(actionEvent, "Start", "../sampleStart.fxml");
        }
    }

    private static Connection conDB() {
        try {
                Class.forName("com.mysql.cj.jdbc.Driver");
//            Connection con =  DriverManager.getConnection("jdbc:mysql://localhost:3306/ia_raspored", "root", "!@bazepodataka1234");
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/ia_raspored?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","root","asus"); // IZMJENE ZA DRUGE KORISNIKE
            System.out.println("Uspjesno povezivanje sa bazom podataka");
            return con;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @FXML private Label lblError;
    @FXML private TextField txtUsername;
    @FXML private PasswordField txtPassword;

    private final Connection conn = conDB();
    // login connects to database and pick up one or nothing rows
    // returned from sql query
    private void login() throws SQLException {
        lblError.setText("");

        username = txtUsername.getText().toString();
        password = txtPassword.getText().toString();

        if(conn == null){
            System.out.println("Neuspjelo povezivanje sa bazom podataka");
        } else {
            ResultSet rows = null;
            Statement stmt = conn.createStatement();
            if (username.length() != 0 && password.length() != 0) {
                // connecting to the database and executing query
                rows = stmt.executeQuery("select username, password, prodekan from nastavnik where username = '" + username + "' and password = '" + password + "'");
                if (!rows.isBeforeFirst()) {
                    lblError.setText("Netacni podaci (KorisnickoIme/Sifra)");
                    txtUsername.setText("");
                    txtPassword.setText("");
                } else {
                    while (rows.next()) {
                        String uname = rows.getString("username");
                        String pw = rows.getString("password");
                        boolean prodekan = rows.getBoolean("prodekan");
                        nastavnik n = new nastavnik(uname, pw, prodekan);
                        System.out.println("Prijavljivanje korisnika: " + username);
                        nastavnici.add(n);
                    }
                }
            } else {
                lblError.setText("Netacni podaci (KorisnickoIme/Sifra)");
                txtUsername.setText("");
                txtPassword.setText("");
            }
        }
    }

    public void akcija(ActionEvent actionEvent, String title, String path) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource(path));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle(title);
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        // TODO Auto-generated method stub

    }

}