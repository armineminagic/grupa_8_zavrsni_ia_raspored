package START.PROFESOR.obrisi_rezervaciju;

import START.ENTITETI.cas;
import START.ENTITETI.rezervacija;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.LocalTime;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.swing.JOptionPane;

   public class mysqlconnect {

        Connection conn = null;
        public static Connection ConnectDb(){
            try {
                Class.forName("com.mysql.jdbc.Driver");
                Connection conn =  DriverManager.getConnection("jdbc:mysql://localhost:3306/ia_raspored", "root", "!@bazepodataka1234");
                return conn;
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
                return null;
            }

        }

        public static ObservableList<rezervacija> getDatarezervacije(){
            Connection conn = ConnectDb();
            ObservableList<rezervacija> list = FXCollections.observableArrayList();
            try {
                PreparedStatement ps = conn.prepareStatement("select * from rezervacija");
                ResultSet rs = ps.executeQuery();

                while (rs.next()){
                    list.add(new rezervacija(rs.getInt(1), rs.getString(2),  rs.getString(3), rs.getString(4), rs.getString(5),
                                  rs.getInt(6), rs.getInt(7)));
                }
            } catch (Exception e) {
            }
            return list;
        }

   }


