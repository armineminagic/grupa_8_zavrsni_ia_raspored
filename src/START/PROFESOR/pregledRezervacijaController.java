package START.PROFESOR;

import START.ENTITETI.rezervacija;
import com.sun.javafx.scene.control.LabeledText;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class pregledRezervacijaController implements Initializable {

    @FXML
    private TableView table;
    @FXML private TableColumn<rezervacija, String> idRezervacija;
    @FXML private TableColumn<rezervacija, String> nazivRezervacija;
    @FXML private TableColumn<rezervacija, String> datumRezervacija;
    @FXML private TableColumn<rezervacija, String> pocetak;
    @FXML private TableColumn<rezervacija, String> kraj;
    @FXML private TableColumn<rezervacija, String> idSala;
    @FXML private TableColumn<rezervacija, String> sifNastavnik;
    @FXML private Label label;

    public void initData(String uname){
        username = uname;
    }

    private String username;

    ObservableList<rezervacija> rezervacije = FXCollections.observableArrayList();

    PreparedStatement pst = null;
    @FXML private TextField txt_id;

    public void initialize(URL url, ResourceBundle rb) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ia_raspored", "root", "!@bazepodataka1234");

            String upit1 = "SELECT * FROM rezervacija";
            Statement statement1 = connection.createStatement();
            ResultSet resultSet1 = statement1.executeQuery(upit1);

            while (resultSet1.next()) {
                rezervacije.add(new rezervacija(resultSet1.getInt(1), resultSet1.getString(2), resultSet1.getString(3),
                        resultSet1.getString(4), resultSet1.getString(5), resultSet1.getInt(6), resultSet1.getInt(7)));
            }
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        idRezervacija.setCellValueFactory(new PropertyValueFactory<>("idRezervacija"));
        nazivRezervacija.setCellValueFactory(new PropertyValueFactory<>("nazivRezervacija"));
        datumRezervacija.setCellValueFactory(new PropertyValueFactory<>("datumRezervacija"));
        pocetak.setCellValueFactory(new PropertyValueFactory<>("pocetak"));
        kraj.setCellValueFactory(new PropertyValueFactory<>("kraj"));
        idSala.setCellValueFactory(new PropertyValueFactory<>("idSala"));
        sifNastavnik.setCellValueFactory(new PropertyValueFactory<>("sifNastavnik"));



        table.setItems(rezervacije);
    }

    public static List<Stage> stagev = new ArrayList<Stage>();
    public void akcija(ActionEvent actionEvent) {
        String text = ((Button) actionEvent.getSource()).getText();;
        stagev.add((Stage) ((Node) actionEvent.getSource()).getScene().getWindow());
        System.out.println(text);
        if (text.equals("Nazad")) {
            nazad(actionEvent, username, "sampleProfesor.fxml");
        }
    }

    public void nazad(ActionEvent actionEvent, String title, String path) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource(path));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle(title);
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}