package START.PROFESOR;

import START.PROFESOR.obrisi_rezervaciju.obrisi_rezervacijuController;
import com.sun.javafx.scene.control.LabeledText;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import START.ENTITETI.*;

import javax.swing.*;

public class profesorController {

    public static List<Stage> stagev = new ArrayList<Stage>();
    public static List<String> fxmlv = new ArrayList<String>();

    private String username;
    public void akcija(ActionEvent actionEvent) {
        String text = ((Button) actionEvent.getSource()).getText();;
        stagev.add((Stage) ((Node) actionEvent.getSource()).getScene().getWindow());
        fxmlv.add("../PROFESOR/sampleProfesor.fxml");
        username = stagev.get(0).getTitle();
        System.out.println(text);
        if (text.equals("Dodaj/Izmijeni/\n" +
                "Izbriši rezervaciju")) {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../PROFESOR/obrisi_rezervaciju/obrisi_rezervaciju.fxml"));
            Parent view = null;
            try{
                view = loader.load();
            } catch (IOException e){
                e.printStackTrace();
            }
            obrisi_rezervacijuController controller = loader.getController();
            controller.initData(username);
            uradi(actionEvent, view, "Obrisi rezervaciju: " + username);
        }
        else if (text.equals("Mjesečni izvještaj")) {
            uradi(actionEvent, "Mjesečni izvještaj: " + username, "../PROFESOR/mjesecni_izvjestaj/mjesecni_izvjestaj.fxml");
        }
        else if (text.equals("Pregled rasporeda")) {
            uradi(actionEvent, username, "../RASPORED/Rasporedsample.fxml");
        }
        else if (text.equals("Pregled rezervacija")) {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("pregledRezervacija.fxml"));
            Parent view = null;
            try{
                view = loader.load();
            } catch (IOException e){
                e.printStackTrace();
            }
            pregledRezervacijaController controller = loader.getController();
            controller.initData(username);
            uradi(actionEvent, view, "Pregled rezervacija: " + username);
        }

        else if (text.equals("Odjavite se")) {
            uradi(actionEvent, "Dobro došli", "../sampleStart.fxml");
        }
    }

    public void uradi(ActionEvent actionEvent, Parent view, String title){
        Scene scene2 = new Scene(view);
        Stage newWindow = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        newWindow.setTitle(title);
        newWindow.centerOnScreen();
        newWindow.setScene(scene2);
        newWindow.show();
    }

    public void uradi(ActionEvent actionEvent, String title, String path) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource(path));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle(title);
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}