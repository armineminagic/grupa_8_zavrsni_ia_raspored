package START.ENTITETI;

import java.sql.Time;
import java.time.LocalTime;

public class cas {
    private int idCas;
    private String tipCas;
    private String dan;
    private String pocetak;//treba time
    private String kraj; //treba time
    private int idGrupa;
    private int sifPredmet;
    private int idSala;
    private String nazPredmet;
    private String nazGrupa;

    public cas(String nazPredmet, String tipCas, String pocetak, String kraj, String dan, String nazGrupa ){
        this.nazPredmet = nazPredmet;
        this.nazGrupa = nazGrupa;
        this.tipCas = tipCas;
        this.pocetak = pocetak;
        this.kraj = kraj;
        this.dan = dan;
    }

    public cas(int idCas, String tipCas, String dan, String pocetak, String kraj, int idGrupa,
               int sifPredmet, int idSala) {
        this.idCas = idCas;
        this.tipCas = tipCas;
        this.dan = dan;
        this.pocetak = pocetak;
        this.kraj = kraj;
        this.idGrupa = idGrupa;
        this.sifPredmet = sifPredmet;
        this.idSala = idSala;
    }

    public int getIdCas() {
        return idCas;
    }

    public void setIdCas(int idCas) {
        this.idCas = idCas;
    }

    public String getTipCas() {
        return tipCas;
    }

    public void setTipCas(String tipCas) {
        this.tipCas = tipCas;
    }

    public String getDan() {
        return dan;
    }

    public void setDan(String dan) {
        this.dan = dan;
    }

    public String getPocetak() {
        return pocetak;
    }

    public void setPocetak(String pocetak) {
        this.pocetak = pocetak;
    }

    public String getKraj() {
        return kraj;
    }

    public void setKraj(String kraj) {
        this.kraj = kraj;
    }

    public int getIdGrupa() {
        return idGrupa;
    }

    public void setIdGrupa(int idGrupa) {
        this.idGrupa = idGrupa;
    }

    public int getSifPredmet() {
        return sifPredmet;
    }

    public void setSifPredmet(int sifPredmet) {
        this.sifPredmet = sifPredmet;
    }

    public int getIdSala() {
        return idSala;
    }

    public void setIdSala(int idSala) {
        this.idSala = idSala;
    }
}