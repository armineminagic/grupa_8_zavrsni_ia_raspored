package START.ENTITETI;

public class grupa {
    private int idGrupa;
    private String nazGrupa;
    private int brojStud;
    private int sifNastavnik;

    public grupa(){}

    public grupa(int idGrupa, String nazGrupa, int brojStud, int sifNastavnik){
        this.idGrupa = idGrupa;
        this.nazGrupa = nazGrupa;
        this.brojStud = brojStud;
        this.sifNastavnik = sifNastavnik;
    }

    public int getIdGrupa() {
        return idGrupa;
    }

    public void setIdGrupa(int idGrupa) {
        this.idGrupa = idGrupa;
    }

    public String getNazGrupa() {
        return nazGrupa;
    }

    public void setNazGrupa(String nazGrupa) {
        this.nazGrupa = nazGrupa;
    }

    public int getBrojStud() {
        return brojStud;
    }

    public void setBrojStud(int brojStud) {
        this.brojStud = brojStud;
    }

    public int getSifNastavnik() {
        return sifNastavnik;
    }

    public void setSifNastavnik(int sifNastavnik) {
        this.sifNastavnik = sifNastavnik;
    }
}
