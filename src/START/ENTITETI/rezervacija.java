package START.ENTITETI;


public class rezervacija {
    private int idRezervacija;
    private String nazivRezervacija;
    private String datumRezervacija;
    private String pocetak;
    private String kraj;
    private int idSala;
    private int sifNastavnik;

    public rezervacija() {
    }

    public rezervacija(int idRezervacija, String nazivRezervacija, String datumRezervacija,
                       String pocetak, String kraj, int idSala, int sifNastavnik) {
        this.idRezervacija = idRezervacija;
        this.nazivRezervacija = nazivRezervacija;
        this.datumRezervacija = datumRezervacija;
        this.pocetak = pocetak;
        this.kraj = kraj;
        this.idSala = idSala;
        this.sifNastavnik = sifNastavnik;
    }

    public int getIdRezervacija() {
        return idRezervacija;
    }

    public void setIdRezervacija(int idRezervacija) {
        this.idRezervacija = idRezervacija;
    }

    public String getDatumRezervacija() {
        return datumRezervacija;
    }

    public void setDatumRezervacija(String datumRezervacija) {
        this.datumRezervacija = datumRezervacija;
    }

    public String getNazivRezervacija() {
        return nazivRezervacija;
    }

    public void setNazivRezervacija(String nazivRezervacija) {
        this.nazivRezervacija = nazivRezervacija;
    }

    public String getPocetak() {
        return pocetak;
    }

    public void setPocetak(String pocetak) {
        this.pocetak = pocetak;
    }

    public String getKraj() {
        return kraj;
    }

    public void setKraj(String kraj) {
        this.kraj = kraj;
    }

    public int getIdSala() {
        return idSala;
    }

    public void setIdSala(int idSala) {
        this.idSala = idSala;
    }

    public int getSifNastavnik() {
        return sifNastavnik;
    }
}