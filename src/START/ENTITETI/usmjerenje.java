package START.ENTITETI;

public class usmjerenje {
    private int idUsmjerenje;
    private String oznUsmjerenje;
    private String nazivUsmjerenje;

    public usmjerenje(){}

    public usmjerenje(int idUsmjerenje, String oznUsmjerenje, String nazivUsmjerenje){
        this.idUsmjerenje = idUsmjerenje;
        this.oznUsmjerenje = oznUsmjerenje;
        this.nazivUsmjerenje = nazivUsmjerenje;
    }

    public int getIdUsmjerenje() {
        return idUsmjerenje;
    }

    public void setIdUsmjerenje(int idUsmjerenje) {
        this.idUsmjerenje = idUsmjerenje;
    }

    public String getOznUsmjerenje() {
        return oznUsmjerenje;
    }

    public void setOznUsmjerenje(String oznUsmjerenje) {
        this.oznUsmjerenje = oznUsmjerenje;
    }

    public String getNazivUsmjerenje() {
        return nazivUsmjerenje;
    }

    public void setNazivUsmjerenje(String nazivUsmjerenje) {
        this.nazivUsmjerenje = nazivUsmjerenje;
    }
}
