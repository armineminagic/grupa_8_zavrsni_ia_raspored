package START.ENTITETI;

public class predmet {
    private int sifPredmet;
    private String nazPredmet;
    private String skrPredmet;
    private String nazivSemestar;

    public predmet(){}

    public predmet(int sifPredmet, String nazPredmet, String skrPredmet, String nazivSemestar){
        this.sifPredmet = sifPredmet;
        this.nazPredmet = nazPredmet;
        this.skrPredmet = skrPredmet;
        this.nazivSemestar = nazivSemestar;
    }

    public int getSifPredmet() {
        return sifPredmet;
    }

    public void setSifPredmet(int sifPredmet) {
        this.sifPredmet = sifPredmet;
    }

    public String getNazPredmet() {
        return nazPredmet;
    }

    public void setNazPredmet(String nazPredmet) {
        this.nazPredmet = nazPredmet;
    }

    public String getSkrPredmet() {
        return skrPredmet;
    }

    public void setSkrPredmet(String skrPredmet) {
        this.skrPredmet = skrPredmet;
    }

    public String getNazivSemestar() {
        return nazivSemestar;
    }

    public void setNazivSemestar(String nazivSemestar) {
        this.nazivSemestar = nazivSemestar;
    }
}
